package se.jobtechdev.demoencryptsign;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import sun.misc.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Service
public class KeyService {
    private final RSAPrivateKey exampleAfPrivateKey;
    private final RSAPublicKey exampleAfPublicKey;
    private final RSAPrivateKey exampleAxaPrivateKey;
    private final RSAPublicKey exampleAxaPublicKey;

    public KeyService() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final FileInputStream afPrivateKeyFile = new FileInputStream(new ClassPathResource("example-af-private.key").getFile());
        final FileInputStream afPublicKeyFile = new FileInputStream(new ClassPathResource("example-af-public.key").getFile());
        final FileInputStream axaPrivateKeyFile = new FileInputStream(new ClassPathResource("example-axa-private.key").getFile());
        final FileInputStream axaPublicKeyFile = new FileInputStream(new ClassPathResource("example-axa-public.key").getFile());

        byte[] afPrivateKeyBytes = IOUtils.readAllBytes(afPrivateKeyFile);
        byte[] afPublicKeyBytes = IOUtils.readAllBytes(afPublicKeyFile);
        byte[] axaPrivateKeyBytes = IOUtils.readAllBytes(axaPrivateKeyFile);
        byte[] axaPublicKeyBytes = IOUtils.readAllBytes(axaPublicKeyFile);

        final PKCS8EncodedKeySpec afPrivateKeySpec = new PKCS8EncodedKeySpec(afPrivateKeyBytes);
        final X509EncodedKeySpec afPublicKeySpec = new X509EncodedKeySpec(afPublicKeyBytes);
        final PKCS8EncodedKeySpec axaPrivateKeySpec = new PKCS8EncodedKeySpec(axaPrivateKeyBytes);
        final X509EncodedKeySpec axaPublicKeySpec = new X509EncodedKeySpec(axaPublicKeyBytes);

        exampleAfPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(afPrivateKeySpec);
        exampleAfPublicKey = (RSAPublicKey) keyFactory.generatePublic(afPublicKeySpec);
        exampleAxaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(axaPrivateKeySpec);
        exampleAxaPublicKey = (RSAPublicKey) keyFactory.generatePublic(axaPublicKeySpec);
    }

    public RSAPrivateKey getExampleAfPrivateKey() {
        return exampleAfPrivateKey;
    }

    public RSAPublicKey getExampleAfPublicKey() {
        return exampleAfPublicKey;
    }

    public RSAPrivateKey getExampleAxaPrivateKey() {
        return exampleAxaPrivateKey;
    }

    public RSAPublicKey getExampleAxaPublicKey() {
        return exampleAxaPublicKey;
    }
}

package se.jobtechdev.demoencryptsign;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;

@Service
public class EncryptionService {
    public EncryptedJWT encrypt(JWTClaimsSet claims, RSAPublicKey publicKey) throws JOSEException {
        final JWEHeader header = new JWEHeader(JWEAlgorithm.RSA_OAEP_512, EncryptionMethod.A128GCM);
        final EncryptedJWT jwe = new EncryptedJWT(header, claims);
        jwe.encrypt(new RSAEncrypter(publicKey));
        return jwe;
    }

    public EncryptedJWT decrypt(EncryptedJWT jwe, PrivateKey privateKey) throws JOSEException {
        jwe.decrypt(new RSADecrypter(privateKey));
        return jwe;
    }
}

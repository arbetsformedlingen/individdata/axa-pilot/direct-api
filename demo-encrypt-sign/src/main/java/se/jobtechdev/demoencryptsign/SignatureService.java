package se.jobtechdev.demoencryptsign;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;

@Service
public class SignatureService {
    public SignedJWT sign(JWTClaimsSet claims, PrivateKey privateKey) throws JOSEException {
        final JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        final SignedJWT jws = new SignedJWT(header, claims);
        jws.sign(new RSASSASigner(privateKey));
        return jws;
    }

    public SignedJWT verify(SignedJWT jws, RSAPublicKey publicKey) throws JOSEException {
        if (!jws.verify(new RSASSAVerifier(publicKey))) {
            throw new Error("Signature verification failed");
        }
        return jws;
    }
}

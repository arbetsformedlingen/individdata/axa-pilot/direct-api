package se.jobtechdev.demoencryptsign;

import com.nimbusds.jose.*;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.text.ParseException;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

@RestController
public class ApiController {
    @Autowired
    KeyService keyService;

    @Autowired
    SignatureService signatureService;

    @Autowired
    EncryptionService encryptionService;

    @PostMapping("/init")
    ResponseEntity<String> init(@RequestBody Body body) throws JOSEException, ParseException {
        final EncryptedJWT decodedJWE = EncryptedJWT.parse(body.encryptedContent);
        final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
        final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
        final String encodedJWS = (String) claimsJWE.getClaim("payload");
        final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
        final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();
        signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey());

        if (claimsJWE.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE id\"}");
        if (claimsJWE.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE issuer\"}");
        if (claimsJWE.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE not before time\"}");
        if (claimsJWE.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE expiration time\"}");
        if (claimsJWS.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS id\"}");
        if (claimsJWS.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS issuer\"}");
        if (claimsJWS.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS not before time\"}");
        if (claimsJWS.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS expiration time\"}");
        if (claimsJWS.getClaim("clientId") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientId\"}");
        if (claimsJWS.getClaim("clientSecret") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientSecret\"}");
        if (claimsJWS.getClaim("loginId") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS loginId\"}");
        if (claimsJWS.getClaim("personalIdentityNumber") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS personalIdentityNumber\"}");

        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @PostMapping("/token")
    ResponseEntity<String> token(@RequestBody Body body) throws JOSEException, ParseException {
        final EncryptedJWT decodedJWE = EncryptedJWT.parse(body.encryptedContent);
        final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
        final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
        final String encodedJWS = (String) claimsJWE.getClaim("payload");
        final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
        final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();
        signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey());

        if (claimsJWE.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE id\"}");
        if (claimsJWE.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE issuer\"}");
        if (claimsJWE.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE not before time\"}");
        if (claimsJWE.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE expiration time\"}");
        if (claimsJWS.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS id\"}");
        if (claimsJWS.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS issuer\"}");
        if (claimsJWS.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS not before time\"}");
        if (claimsJWS.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS expiration time\"}");
        if (claimsJWS.getClaim("clientId") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientId\"}");
        if (claimsJWS.getClaim("clientSecret") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientSecret\"}");
        if (claimsJWS.getClaim("authCode") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS authCode\"}");

        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("accessToken", UUID.randomUUID().toString())
                .claim("refreshToken", UUID.randomUUID().toString())
                .claim("accessTokenExpirationDate", new Date().toInstant().truncatedTo(ChronoUnit.SECONDS))
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @PostMapping("/refresh")
    ResponseEntity<String> refresh(@RequestBody Body body) throws JOSEException, ParseException {
        final EncryptedJWT decodedJWE = EncryptedJWT.parse(body.encryptedContent);
        final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
        final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
        final String encodedJWS = (String) claimsJWE.getClaim("payload");
        final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
        final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();
        signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey());

        if (claimsJWE.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE id\"}");
        if (claimsJWE.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE issuer\"}");
        if (claimsJWE.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE not before time\"}");
        if (claimsJWE.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE expiration time\"}");
        if (claimsJWS.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS id\"}");
        if (claimsJWS.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS issuer\"}");
        if (claimsJWS.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS not before time\"}");
        if (claimsJWS.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS expiration time\"}");
        if (claimsJWS.getClaim("clientId") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientId\"}");
        if (claimsJWS.getClaim("clientSecret") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS clientSecret\"}");
        if (claimsJWS.getClaim("refreshToken") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS refreshToken\"}");

        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("accessToken", UUID.randomUUID().toString())
                .claim("refreshToken", UUID.randomUUID().toString())
                .claim("accessTokenExpirationDate", new Date().toInstant().truncatedTo(ChronoUnit.SECONDS))
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @PostMapping("/status")
    ResponseEntity<String> status(@RequestBody Body body) throws JOSEException, ParseException {
        final EncryptedJWT decodedJWE = EncryptedJWT.parse(body.encryptedContent);
        final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
        final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
        final String encodedJWS = (String) claimsJWE.getClaim("payload");
        final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
        final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();
        signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey());

        if (claimsJWE.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE id\"}");
        if (claimsJWE.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE issuer\"}");
        if (claimsJWE.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE not before time\"}");
        if (claimsJWE.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWE expiration time\"}");
        if (claimsJWS.getJWTID() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS id\"}");
        if (claimsJWS.getIssuer() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS issuer\"}");
        if (claimsJWS.getNotBeforeTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS not before time\"}");
        if (claimsJWS.getExpirationTime() == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS expiration time\"}");
        if (claimsJWS.getClaim("accessToken") == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\": \"Missing JWS accessToken\"}");

        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("status", "Unemployed")
                .claim("registrationDate", "2022-01-01")
                .claim("statusChangedDate", "2022-01-05")
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("Arbetsförmedlingen")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @GetMapping("/sampleTokenRequest")
    ResponseEntity<String> sampleTokenRequest() throws JOSEException {
        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("clientId", "example-client-id")
                .claim("clientSecret", "example-client-secret")
                .claim("authCode", UUID.randomUUID().toString())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @GetMapping("/sampleRefreshRequest")
    ResponseEntity<String> sampleRefreshRequest() throws JOSEException {
        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("clientId", "example-client-id")
                .claim("clientSecret", "example-client-secret")
                .claim("refreshToken", UUID.randomUUID().toString())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }

    @GetMapping("/sampleStatusRequest")
    ResponseEntity<String> sampleStatusRequest() throws JOSEException {
        final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("accessToken", UUID.randomUUID().toString())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAxaPrivateKey());

        final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                .jwtID(UUID.randomUUID().toString())
                .issuer("AXA")
                .claim("payload", signedJWS.serialize())
                .notBeforeTime(Date.from(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS)))
                .expirationTime(Date.from(new Date().toInstant().plus(1, ChronoUnit.HOURS).truncatedTo(ChronoUnit.SECONDS)))
                .build(), keyService.getExampleAfPublicKey());

        return ResponseEntity.status(HttpStatus.OK).body("{\"encryptedContent\": \"" + encryptedJWE.serialize() + "\"}");
    }
}

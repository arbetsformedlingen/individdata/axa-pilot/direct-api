package se.jobtechdev.demoencryptsign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoEncryptSignApplication {
    public static void main(String args[]) {
        SpringApplication.run(DemoEncryptSignApplication.class, args);
    }
}

package se.jobtechdev.demoencryptsign;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EncryptionDecryptionTest {
    @Autowired
    KeyService keyService;

    @Autowired
    SignatureService signatureService;

    @Autowired
    EncryptionService encryptionService;

    @Test
    void InitRequestTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "AXA";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_CLIENT_ID = "clientId";
        final String CLAIM_CLIENT_ID_VALUE = "example-client-id";
        final String CLAIM_CLIENT_SECRET = "clientSecret";
        final String CLAIM_CLIENT_SECRET_VALUE = "abcd1234";
        final String CLAIM_LOGIN_ID = "loginId";
        final String CLAIM_LOGIN_ID_VALUE = "axa1234";
        final String CLAIM_PERSONAL_IDENTITY_NUMBER = "personalIdentityNumber";
        final String CLAIM_PERSONAL_IDENTITY_NUMBER_VALUE = "190101019999";
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encryptedContent;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_CLIENT_ID, CLAIM_CLIENT_ID_VALUE)
                    .claim(CLAIM_CLIENT_SECRET, CLAIM_CLIENT_SECRET_VALUE)
                    .claim(CLAIM_LOGIN_ID, CLAIM_LOGIN_ID_VALUE)
                    .claim(CLAIM_PERSONAL_IDENTITY_NUMBER, CLAIM_PERSONAL_IDENTITY_NUMBER_VALUE)
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPublicKey());

            encryptedContent = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encryptedContent);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_CLIENT_ID_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_ID));
            assertEquals(CLAIM_CLIENT_SECRET_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_SECRET));
            assertEquals(CLAIM_LOGIN_ID_VALUE, claimsJWS.getClaim(CLAIM_LOGIN_ID));
            assertEquals(CLAIM_PERSONAL_IDENTITY_NUMBER_VALUE, claimsJWS.getClaim(CLAIM_PERSONAL_IDENTITY_NUMBER));
        }
    }

    @Test
    void TokenRequestTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "AXA";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_CLIENT_ID = "clientId";
        final String CLAIM_CLIENT_ID_VALUE = "example-client-id";
        final String CLAIM_CLIENT_SECRET = "clientSecret";
        final String CLAIM_CLIENT_SECRET_VALUE = "abcd1234";
        final String CLAIM_AUTH_CODE = "authCode";
        final String CLAIM_AUTH_CODE_VALUE = "cccccccc-cccc-cccc-cccc-cccccccccccc";
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encryptedContent;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_CLIENT_ID, CLAIM_CLIENT_ID_VALUE)
                    .claim(CLAIM_CLIENT_SECRET, CLAIM_CLIENT_SECRET_VALUE)
                    .claim(CLAIM_AUTH_CODE, CLAIM_AUTH_CODE_VALUE)
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPublicKey());

            encryptedContent = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encryptedContent);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_CLIENT_ID_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_ID));
            assertEquals(CLAIM_CLIENT_SECRET_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_SECRET));
            assertEquals(CLAIM_AUTH_CODE_VALUE, claimsJWS.getClaim(CLAIM_AUTH_CODE));
        }
    }

    @Test
    void TokenResponseTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "Arbetsförmedlingen";
        final String SUBJECT = "19910101-0101";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_ACCESS_TOKEN = "accessToken";
        final String CLAIM_ACCESS_TOKEN_VALUE = "aaaaaaaa";
        final String CLAIM_REFRESH_TOKEN = "refreshToken";
        final String CLAIM_REFRESH_TOKEN_VALUE = "bbbbbbbb";
        final String CLAIM_ACCESS_TOKEN_EXPIRATION_DATE = "accessTokenExpirationDate";
        final Instant CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encodedJwe;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .subject(SUBJECT)
                    .claim(CLAIM_ACCESS_TOKEN, CLAIM_ACCESS_TOKEN_VALUE)
                    .claim(CLAIM_REFRESH_TOKEN, CLAIM_REFRESH_TOKEN_VALUE)
                    .claim(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE, Date.from(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE))
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPublicKey());

            encodedJwe = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encodedJwe);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAxaPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAfPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(SUBJECT, claimsJWS.getSubject());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_ACCESS_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_ACCESS_TOKEN));
            assertEquals(CLAIM_REFRESH_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_REFRESH_TOKEN));
            assertEquals(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE, Instant.ofEpochSecond((long)claimsJWS.getClaim(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE)));
        }
    }

    @Test
    void TokenRefreshRequestTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "AXA";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_CLIENT_ID = "clientId";
        final String CLAIM_CLIENT_ID_VALUE = "example-client-id";
        final String CLAIM_CLIENT_SECRET = "clientSecret";
        final String CLAIM_CLIENT_SECRET_VALUE = "abcd1234";
        final String CLAIM_REFRESH_TOKEN = "refreshToken";
        final String CLAIM_REFRESH_TOKEN_VALUE = "cccccccc-cccc-cccc-cccc-cccccccccccc";
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encryptedContent;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_CLIENT_ID, CLAIM_CLIENT_ID_VALUE)
                    .claim(CLAIM_CLIENT_SECRET, CLAIM_CLIENT_SECRET_VALUE)
                    .claim(CLAIM_REFRESH_TOKEN, CLAIM_REFRESH_TOKEN_VALUE)
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPublicKey());

            encryptedContent = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encryptedContent);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_CLIENT_ID_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_ID));
            assertEquals(CLAIM_CLIENT_SECRET_VALUE, claimsJWS.getClaim(CLAIM_CLIENT_SECRET));
            assertEquals(CLAIM_REFRESH_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_REFRESH_TOKEN));
        }
    }

    @Test
    void TokenRefreshResponseTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "Arbetsförmedlingen";
        final String SUBJECT = "19910101-0101";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_ACCESS_TOKEN = "accessToken";
        final String CLAIM_ACCESS_TOKEN_VALUE = "aaaaaaaa";
        final String CLAIM_REFRESH_TOKEN = "refreshToken";
        final String CLAIM_REFRESH_TOKEN_VALUE = "bbbbbbbb";
        final String CLAIM_ACCESS_TOKEN_EXPIRATION_DATE = "accessTokenExpirationDate";
        final Instant CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encodedJwe;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .subject(SUBJECT)
                    .claim(CLAIM_ACCESS_TOKEN, CLAIM_ACCESS_TOKEN_VALUE)
                    .claim(CLAIM_REFRESH_TOKEN, CLAIM_REFRESH_TOKEN_VALUE)
                    .claim(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE, Date.from(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE))
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPublicKey());

            encodedJwe = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encodedJwe);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAxaPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAfPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(SUBJECT, claimsJWS.getSubject());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_ACCESS_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_ACCESS_TOKEN));
            assertEquals(CLAIM_REFRESH_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_REFRESH_TOKEN));
            assertEquals(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE_VALUE, Instant.ofEpochSecond((long)claimsJWS.getClaim(CLAIM_ACCESS_TOKEN_EXPIRATION_DATE)));
        }
    }

    @Test
    void StatusRequestTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "AXA";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_ACCESS_TOKEN = "accessToken";
        final String CLAIM_ACCESS_TOKEN_VALUE = "abcd1234abcd1234";
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encryptedContent;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_ACCESS_TOKEN, CLAIM_ACCESS_TOKEN_VALUE)
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPublicKey());

            encryptedContent = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encryptedContent);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAfPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAxaPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_ACCESS_TOKEN_VALUE, claimsJWS.getClaim(CLAIM_ACCESS_TOKEN));
        }
    }

    @Test
    void StatusResponseTest() throws JOSEException, ParseException {
        final String JWS_ID = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        final String JWE_ID = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb";
        final String ISSUER = "Arbetsförmedlingen";
        final String CLAIM_PAYLOAD = "payload";
        final String CLAIM_STATUS = "status";
        final String CLAIM_STATUS_VALUE = "Unemployed";
        final String CLAIM_REGISTRATION_DATE = "registrationDate";
        final String CLAIM_REGISTRATION_DATE_VALUE = "2022-01-01";
        final String CLAIM_STATUS_CHANGED_DATE = "statusChangedDate";
        final String CLAIM_STATUS_CHANGED_DATE_VALUE = "2022-01-05";
        final Instant NOT_BEFORE_TIME = new Date().toInstant().truncatedTo(ChronoUnit.SECONDS);
        final Instant EXPIRATION_TIME = NOT_BEFORE_TIME.plus(1, ChronoUnit.HOURS);

        String encodedJwe;
        {
            final SignedJWT signedJWS = signatureService.sign(new JWTClaimsSet.Builder()
                    .jwtID(JWS_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_STATUS, CLAIM_STATUS_VALUE)
                    .claim(CLAIM_REGISTRATION_DATE, CLAIM_REGISTRATION_DATE_VALUE)
                    .claim(CLAIM_STATUS_CHANGED_DATE, CLAIM_STATUS_CHANGED_DATE_VALUE)
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAfPrivateKey());

            final EncryptedJWT encryptedJWE = encryptionService.encrypt(new JWTClaimsSet.Builder()
                    .jwtID(JWE_ID)
                    .issuer(ISSUER)
                    .claim(CLAIM_PAYLOAD, signedJWS.serialize())
                    .notBeforeTime(Date.from(NOT_BEFORE_TIME))
                    .expirationTime(Date.from(EXPIRATION_TIME))
                    .build(), keyService.getExampleAxaPublicKey());

            encodedJwe = encryptedJWE.serialize();
        }

        {
            final EncryptedJWT decodedJWE = EncryptedJWT.parse(encodedJwe);
            final EncryptedJWT decryptedJWE = encryptionService.decrypt(decodedJWE, keyService.getExampleAxaPrivateKey());
            final JWTClaimsSet claimsJWE = decryptedJWE.getJWTClaimsSet();
            final String encodedJWS = (String) claimsJWE.getClaim(CLAIM_PAYLOAD);
            final SignedJWT decodedJWS = SignedJWT.parse(encodedJWS);
            final JWTClaimsSet claimsJWS = decodedJWS.getJWTClaimsSet();

            assertEquals(JWE_ID, claimsJWE.getJWTID());
            assertEquals(ISSUER, claimsJWE.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWE.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWE.getExpirationTime());
            assertDoesNotThrow(() -> signatureService.verify(decodedJWS, keyService.getExampleAfPublicKey()));
            assertEquals(JWS_ID, claimsJWS.getJWTID());
            assertEquals(ISSUER, claimsJWS.getIssuer());
            assertEquals(Date.from(NOT_BEFORE_TIME), claimsJWS.getNotBeforeTime());
            assertEquals(Date.from(EXPIRATION_TIME), claimsJWS.getExpirationTime());
            assertEquals(CLAIM_STATUS_VALUE, claimsJWS.getClaim(CLAIM_STATUS));
            assertEquals(CLAIM_REGISTRATION_DATE_VALUE, claimsJWS.getClaim(CLAIM_REGISTRATION_DATE));
            assertEquals(CLAIM_STATUS_CHANGED_DATE_VALUE, claimsJWS.getClaim(CLAIM_STATUS_CHANGED_DATE));
        }
    }
}
